package com.example.groeiprojectui2.model

import com.example.groeiprojectui2.R
import java.text.SimpleDateFormat
import java.util.*

//fun getRaceteams(): Array<Raceteam> {
//    val rt1 = Raceteam(1,"Aston Martin Red Bull Racing", GregorianCalendar(2005,Calendar.MARCH,6) ,62,"Formula 1", R.drawable.redbull)
//    val rt2 = Raceteam(2,"Scuderia Ferrari", GregorianCalendar(1950,Calendar.MAY,21),237, "Formula 1", R.drawable.ferrari)
//    val rt3 = Raceteam(3,"Mercedes AMG Petronas F1 Team",GregorianCalendar(1954,Calendar.JULY,4),102, "Formula 1",R.drawable.mercedes)
//    val rt4 = Raceteam(4, "Hyundai Motorsport", GregorianCalendar(2000,Calendar.JANUARY,21), 15, "Rally",R.drawable.hyundai)
//    val rt5 = Raceteam(5, "McLaren F1 Team", GregorianCalendar(1966,Calendar.MAY,22), 182, "Formula 1",R.drawable.mclarenf1)
//    val rt6 = Raceteam(6, "Renault Sport Formula One Team", GregorianCalendar(1977,Calendar.JULY,16), 35, "Formula 1",R.drawable.renault)
//    val rt7 = Raceteam(7, "Yamaha Factory Racing", GregorianCalendar(1999,Calendar.MARCH,18), 105, "MotoGP",R.drawable.yamaha)
//    val rt8 = Raceteam(8, "Rebellion Racing", GregorianCalendar(2010,Calendar.JUNE,6), 15, "Endurance",R.drawable.rebellion)
//    val rt9 = Raceteam( 9, "ROKiT Williams Racing", GregorianCalendar(1975,Calendar.JANUARY,12), 114, "Formula 1",R.drawable.williams)
//    val rt10 = Raceteam(10, "Alfa Romeo Racing", GregorianCalendar(1950,Calendar.MAY,13), 10, "Formula 1",R.drawable.alfaromeo)
//    val rt11 = Raceteam(11, "Scuderia Toro Rosso", GregorianCalendar(2006,Calendar.MARCH,12), 1, "Formula 1",R.drawable.tororosso)
//    val rt12 = Raceteam(12, "Audi Sport ABT Schaeffler", GregorianCalendar(2013,Calendar.JANUARY,20), 12, "Formula E",R.drawable.audi)
//    val rt13 = Raceteam(13, "Nissan E.Dams", GregorianCalendar(2014,Calendar.JANUARY,15), 16, "Formula E",R.drawable.nissan)
//    val rt14 = Raceteam(14, "Dale Coyne Racing", GregorianCalendar(1984,Calendar.MARCH,31), 6, "IndyCar",R.drawable.dalecoyne)
//    val rt15 = Raceteam(15, "Arrow McLaren SP", GregorianCalendar(2001,Calendar.MARCH,18), 7, "IndyCar",R.drawable.arrowmclaren)
//
//    return arrayOf(rt1,rt2,rt3,rt4,rt5,rt6,rt7,rt8,rt9,rt10,rt11,rt12,rt13,rt14,rt15)
//}


fun convertDate(date:GregorianCalendar):String{
    val formatter = SimpleDateFormat("dd/MM/yyyy",Locale.US)
    println(date.time)
    return formatter.format(date.time)
}