package com.example.groeiprojectui2.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.model.Coureur
import kotlinx.android.synthetic.main.coureur_list_item.view.*

class CoureursAdapter(
    private val coureurs: Array<Coureur>
): RecyclerView.Adapter<CoureursAdapter.CoureurViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):CoureurViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val coureurView = inflater.inflate(R.layout.coureur_list_item,parent,false)
        return CoureurViewHolder(coureurView)
    }

    override fun getItemCount(): Int {
        return coureurs.size
    }

    override fun onBindViewHolder(holder: CoureurViewHolder, position: Int) {
        val coureur = coureurs[position]
        holder.first_name.text=coureur.first_name
        holder.last_name.text=coureur.last_name
        holder.birth_day.text=coureur.birth_day
        holder.races.text=coureur.races.toString()
        holder.victories.text=coureur.victories.toString()
    }

    class CoureurViewHolder(view: View):RecyclerView.ViewHolder(view){
        val first_name: TextView = view.first_name
        val last_name: TextView = view.last_name
        val birth_day: TextView = view.birth_day
        val races: TextView = view.races
        val victories: TextView = view.victories
    }
}