package com.example.groeiprojectui2.rest

import android.content.Context
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import com.example.groeiprojectui2.model.Coureur
import com.example.groeiprojectui2.model.Raceteam
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


const val BASE_URL = "http://10.0.2.2:3000"

class RestClient(private val context: Context) {


    @Suppress("DEPRECATION")
    private fun connect(urlString: String): HttpURLConnection {
        val connMgr = context.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        if (networkInfo != null && networkInfo.isConnected) {
            val url = URL(urlString)
            val connection = url.openConnection() as HttpURLConnection
            connection.apply {
                connectTimeout = 15000
                readTimeout = 10000
                requestMethod = "GET"
                connect()
            }
            return connection
        }
        throw IOException("Unable to connect to network")
    }

    fun getTeam(id: Int): Observable<Raceteam> {
        val obs = Observable.create<Raceteam> { emitter ->
            try {
                var connection = connect("${BASE_URL}/raceteams/${id + 1}")
                val gson = GsonBuilder().create()


                val team = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    Raceteam::class.java
                )
                team.apply {
                    connection = connect("${BASE_URL}/${team.image}")
                    team.imageBitmap = BitmapFactory.decodeStream(connection.inputStream)
                }
                emitter.onNext(team)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return obs

    }


    fun getTeams(): Observable<Array<Raceteam>> {
        val observable = Observable.create<Array<Raceteam>> { emitter ->
            try {
                var connection = connect("${BASE_URL}/raceteams")
                val gson = GsonBuilder().create()
                val teams = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    Array<Raceteam>::class.java

                )
                teams.forEach() {
                    connection = connect("${BASE_URL}/${it.image}")
                    it.imageBitmap = BitmapFactory.decodeStream(connection.inputStream)

                }
                emitter.onNext(teams)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return observable

    }

    fun getCoureurs(raceteamId: Int): Observable<Array<Coureur>> {
        val observable = Observable.create<Array<Coureur>> { emitter ->
            try {
                var connection = connect("${BASE_URL}/raceteams/${raceteamId}/coureurs")
                val gson = GsonBuilder().create()
                val coureurs = gson.fromJson(
                    InputStreamReader(connection.inputStream),
                    Array<Coureur>::class.java
                )
                emitter.onNext(coureurs)

            } catch (e: Exception) {
                emitter.onError(e)
            }
        }
        return observable
    }

}