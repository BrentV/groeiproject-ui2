package com.example.groeiprojectui2.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewParent
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.model.Raceteam
import com.example.groeiprojectui2.model.convertDate
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_recycler_item.view.*

class TeamAdapter (
    private val teams: Array<Raceteam>,
    private val context:Context,
    private val teamSelectionListener: TeamSelectionListener
) : RecyclerView.Adapter<TeamAdapter.TeamViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder{
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.activity_recycler_item,parent,false)
        return TeamViewHolder(view)
    }
    override fun getItemCount() = teams.size

    override fun getItemViewType(position: Int): Int {
        return R.layout.activity_recycler_item
    }

    override fun onBindViewHolder(vh: TeamViewHolder, position: Int) {
        val observable = RestClient(context)
            .getTeam(position)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(){team ->
                vh.logo.setImageBitmap(team.imageBitmap)
                vh.team.text=team.name
                vh.date.text=team.start_date
                vh.wins.text=team.wins.toString()
            }

        vh.itemView.setOnClickListener {
            teamSelectionListener.onBrandSelected(position)
        }
    }


    interface TeamSelectionListener {
        fun onBrandSelected(position: Int)
    }

    class TeamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val team: TextView = view.item_team
        val date: TextView = view.item_date
        val wins: TextView = view.item_wins
        val logo: ImageView = view.item_logo
    }
}