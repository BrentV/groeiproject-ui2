package com.example.groeiprojectui2.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.adapters.TeamAdapter
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.Exception

class RecyclerFragment: Fragment() {
    private lateinit var listener: TeamAdapter.TeamSelectionListener
    private lateinit var disposable: Disposable

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is TeamAdapter.TeamSelectionListener)
            listener = context
        else
            throw Exception()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_recycler,container,false)
        disposable = RestClient(requireActivity().applicationContext)
            .getTeams()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(){teams ->
                view.findViewById<RecyclerView>(R.id.rvData).apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = TeamAdapter(teams,context,listener)
                }
            }

        return view
    }

    override fun onDestroy() {
        if(::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}