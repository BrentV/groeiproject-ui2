package com.example.groeiprojectui2.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class ImageActivity : AppCompatActivity() {
    private lateinit var image: ImageView
    private var currentIndex: Int = 0
    private lateinit var disposable:Disposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        currentIndex = intent.getIntExtra("currentIndex",0)
        displayTeam()


    }

    private fun displayTeam(){
        image = findViewById(R.id.expLogo)

        disposable = RestClient(this@ImageActivity)
            .getTeam(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(){team->
                this.image.setImageBitmap(team.imageBitmap)
            }
    }

    override fun onDestroy() {
        if(::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}
