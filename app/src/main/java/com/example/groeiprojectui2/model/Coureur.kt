package com.example.groeiprojectui2.model

import java.util.*

data class Coureur(
    val id:Int,
    val first_name: String,
    val last_name: String,
    val birth_day:String,
    val races:Int,
    val victories: Int,
    val raceTeamId:Int
){

}