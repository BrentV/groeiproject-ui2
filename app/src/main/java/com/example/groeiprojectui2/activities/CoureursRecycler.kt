package com.example.groeiprojectui2.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.adapters.CoureursAdapter
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class CoureursRecycler: AppCompatActivity(){
    private lateinit var disposable: Disposable
    private lateinit var rvCoureurs:RecyclerView
    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState:Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coureurs_recycler)
        currentIndex = intent.getIntExtra("currentIndex",0)
        initializeViews()

        disposable = RestClient(this)
            .getTeam(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{ team ->
                val dis = RestClient(this)
                    .getCoureurs(team.id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe{ coureurs->
                        rvCoureurs.apply {
                            layoutManager = LinearLayoutManager(this@CoureursRecycler)
                            adapter = CoureursAdapter(coureurs)
                        }
                        this.title=team.name
                    }
            }
    }

    private fun initializeViews(){
        rvCoureurs = findViewById(R.id.rvCoureurs)
    }

    override fun onDestroy() {
        if(::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }
}