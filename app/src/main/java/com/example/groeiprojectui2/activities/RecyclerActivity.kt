package com.example.groeiprojectui2.activities

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.adapters.TeamAdapter
import com.example.groeiprojectui2.fragments.DetailFragment

class RecyclerActivity : AppCompatActivity() , TeamAdapter.TeamSelectionListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler)
    }


    override fun onBrandSelected(position: Int) {
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra("currentIndex", position)
            startActivity(intent)
        } else {
            val detailFrag = supportFragmentManager
                .findFragmentById(R.id.detail_land_frag)
                    as DetailFragment
            detailFrag.setTeamIndex(position)
        }
    }


}