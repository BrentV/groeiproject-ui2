package com.example.groeiprojectui2.model

import android.graphics.Bitmap
import android.widget.ImageView
import java.util.*

data class Raceteam(
    val id: Int,
    val name: String,
    val start_date: String,
    val wins: Int,
    val category: String,
    val image: String,
    var imageBitmap: Bitmap
)