package com.example.groeiprojectui2.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {
    private lateinit var image: ImageView
    private lateinit var name: TextView
    private lateinit var startDate:TextView
    private lateinit var wins:TextView
    private lateinit var category:TextView
    //private lateinit var btnNext:Button
    //private lateinit var btnPrevious:Button
    private lateinit var coureursButton: Button
    private lateinit var disposable: Disposable


    private var currentIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        currentIndex = intent.getIntExtra("currentIndex",0)
        initialiseViews()
        addEventHandlers()
        displayItem()
    }

    private fun initialiseViews(){
        image = findViewById(R.id.imageLogo)
        name=findViewById(R.id.nameVal)
        startDate=findViewById(R.id.dateVal)
        wins=findViewById(R.id.winsVal)
        category=findViewById(R.id.categoryVal)
        coureursButton = findViewById(R.id.coureursButton)
    }

    private fun nextTeam(){
        if (currentIndex < 14){
            currentIndex++
            displayItem()
        }else{
            currentIndex = 14
            displayItem()
        }
    }

    private fun previousTeam(){
        if(currentIndex > 0){
            currentIndex--
            displayItem()
        }else{
            currentIndex = 0
            displayItem()
        }
    }

    private fun addEventHandlers() {
//        btnNext = findViewById(R.id.btnNext)
//        btnPrevious = findViewById(R.id.btnPrevious)
//        btnNext.setOnClickListener {
//            nextTeam()
//        }
//        btnPrevious.setOnClickListener { previousTeam() }
        coureursButton.setOnClickListener{
            val coureurIntent = Intent(this,CoureursRecycler::class.java)
            coureurIntent.putExtra("currentIndex",currentIndex)
            startActivity(coureurIntent)
        }

        image.setOnClickListener {
            val intent = Intent(this, ImageActivity::class.java)
            intent.putExtra("currentIndex", currentIndex)
            startActivity(intent)
        }
    }

    private fun displayItem() {
        disposable = RestClient(this@MainActivity)
            .getTeam(currentIndex)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(){team ->
                this.image.setImageBitmap(team.imageBitmap)
                this.name.text = team.name
                this.startDate.text = team.start_date
                this.wins.text = team.wins.toString()
                this.category.text = team.category
                this.coureursButton.setOnClickListener{
                    val coureursIntent = Intent(this,CoureursRecycler::class.java)
                    coureursIntent.putExtra("currentIndex",currentIndex)
                    startActivity(coureursIntent)
                }
            }
    }

    override fun onDestroy(){
        if(::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }



}



