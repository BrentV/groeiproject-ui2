package com.example.groeiprojectui2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.groeiprojectui2.R
import com.example.groeiprojectui2.activities.CoureursRecycler
import com.example.groeiprojectui2.model.Raceteam
import com.example.groeiprojectui2.model.convertDate
import com.example.groeiprojectui2.rest.RestClient
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.view.*

class DetailFragment: Fragment() {
    private lateinit var logo: ImageView
    private lateinit var teamName: TextView
    private lateinit var startDate: TextView
    private lateinit var wins: TextView
    private lateinit var disposable: Disposable
    private lateinit var coureursButton: Button
    private var index: Int = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.activity_main, container, false)

        logo = view.findViewById(R.id.imageLogo)
        teamName = view.findViewById(R.id.nameVal)
        startDate = view.findViewById(R.id.dateVal)
        wins = view.findViewById(R.id.winsVal)
        coureursButton = view.findViewById(R.id.coureursButton)

        updateFields()
        return view
    }


    private fun updateFields() {
        disposable = RestClient(requireContext())
            .getTeam(index)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(){team ->
                this.logo.setImageBitmap(team.imageBitmap)
                this.teamName.text=team.name
                this.startDate.text=team.start_date
                this.wins.text=team.wins.toString()
                this.coureursButton.setOnClickListener(){
                    val coureursIntent=Intent(requireContext(),CoureursRecycler::class.java)
                    coureursIntent.putExtra("currentIndex",index)
                    startActivity(coureursIntent)
                }

            }

    }


    fun setTeamIndex(teamIndex: Int) {
        index = teamIndex
        updateFields()
    }

    override fun onDestroy() {
        if(::disposable.isInitialized)
            disposable.dispose()

        super.onDestroy()
    }



}